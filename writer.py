import os.path
import json


class Writer:
    """
    This class write the output to a json file.

    Internally it contains a dictionary with COICOP codes as key.
    The value is a Touple of a flag indicating whether the code was rejected
    (manual matching failed), and the list of Nace Codes that were matched.
    Can be empty, if manual matching failed.

    It provides a method to check if a COICOP code is already defined,
    one to add a dictionary entry, one  to read and one to write the dict.
    """

    processed = {}

    def __init__(self, path: str='./matches.json') -> None:
        """
        :param path: The path indicates where the output json file should be stored.
        :type path: str
        """
        self.path = path

    def read(self) -> None:
        """
        Checks if the file exists, if it does read it in.
        """

        if os.path.isfile(self.path):
            with open(self.path, 'r') as ifile:
                self.processed = json.load(ifile)

    def write(self) -> None:
        """
        This method writes the dictionary to a file.
        """

        with open(self.path, 'w') as ofile:
            json.dump(self.processed, ofile, sort_keys=True, indent=4)

    def has_coicop(self, coicop: str) -> bool:
        """
        This method checks if a coicop is already processed.

        :param coicop: The coicop code that should be checked.
        :type coicop: str
        :return: If coicop is processed
        :rtype: bool
        """
        print(type(coicop))
        print("coicop: -{}-".format(coicop))
        print(str(coicop in self.processed))
        print(self.processed)
        return coicop in self.processed

    def set_coicop(self, code: str, name: str, naces, manual_naces) -> None:
        """
        This method adds the coicop to the dictionary.
        If naces is None the method assumes that the code was flagged for manual
        matching and therefor sets the manual field to true.

        :param code: The coicop code
        :param naces: The matched NACEs.
            From this parameter the rejected information is inferred.
        """

        print(code)

        nace_out = []
        if naces:
            nace_out = [{'code': nace[1], 'name': nace[0]} for nace in naces]
        if manual_naces:
            nace_out += [{'code': nace} for nace in manual_naces if nace != ""]

        if nace_out:
            self.processed[code] = {'name': name, 'rejected': False, 'nace': nace_out, 'name_fixed': True}
        else:
            self.processed[code] = {'name': name, 'rejected': True, 'nace': [], 'name_fixed': True}
