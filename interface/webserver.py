import flask
from typing import List, Dict


import interface.interface


class EndpointAction(object):

    def __init__(self, action, json: bool=False):
        self.action = action
        self.json = json

    def __call__(self, *args):
        # Perform the action
        answer = self.action()
        # Create the answer (bundle it in a correctly formatted HTTP answer)
        if self.json:
            return flask.jsonify(answer)
        else:
            self.response = flask.Response(answer, status=200, headers={})
            return self.response


class WebInterface(object):
    """
    This class provides the webinterface.

    It builds a flask app that servers a static html file with a small
    javascript inside that requests, displays the data and returns the data.
    """

    def __init__(self, __name__: str, interface: interface.interface.Interface):
        self.app = flask.Flask(__name__, static_url_path='/static/')
        self.interface = interface

        # I need to read in the files, since for some reason send static file does not work.
        with open('./static/index.htm', 'r') as ifile:
            self.root_file = ifile.read()
        with open('./static/display.js', 'r') as ifile:
            self.display_file = ifile.read()

    def run(self):
        self.add_all_endpoints()
        self.app.run()

    def add_all_endpoints(self) -> None:
        """
        This method sets up all endpoints.
        """
        # Add root endpoint
        self.add_endpoint(endpoint="/", endpoint_name="/", handler=self.index)

        # the endpoint that checks if there is more work to do.
        self.add_endpoint(endpoint='/next', endpoint_name='/next', handler=self.check_next, json=True)

        # Add data endpoints
        self.add_endpoint(endpoint="/data", endpoint_name="/data", handler=self.data, json=True)

        self.add_endpoint(endpoint='/display.js', endpoint_name='/display.js', handler=self.display_js)

        self.add_endpoint(endpoint='/write_signal', endpoint_name='/write_signal', handler=self.write_signal)

        # and finally the endpoint that receives the post data.
        # not added with the function below, but directly.
        self.app.add_url_rule('/selection', '/selection', EndpointAction(self.read_selection), methods=['POST'])

    def add_endpoint(self, endpoint=None, endpoint_name=None, handler=None, json: bool=False) -> None:
        """
        This method sets up a single endpoint.
        :param endpoint:
        :param endpoint_name:
        :param handler:
        """
        self.app.add_url_rule(endpoint, endpoint_name, EndpointAction(handler, json))
        # You can also add options here : "... , methods=['POST'], ... "

    def read_selection(self):
        data = flask.request.json
        self.interface.write_data(data)
        return None

    def index(self):
        """
        This method returns the start page.

        :return: a static url page
        """
#        return 'Hey there people'
        return self.root_file

    def check_next(self) -> Dict:
        """
        This function checks if the interface has a further coicop.
        The response is a Dictionary with a single element, response, that is bool.

        :return: A dict so that it is converted to json and sent properly.
        """
        res = self.interface.check_further()

        return {'response': res}

    def data(self) -> Dict:

        data = self.interface.get_selection()
        return data

    def display_js(self):
        return self.display_file

    def write_signal(self) -> None:
        """
        This metho simply passes on a write signal to the main interface process.
        """

        self.interface.write_signal()