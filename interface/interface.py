from typing import Dict, List

import matcher
import writer
import reader.waegungsschema


class Interface:
    """
    This class provides the interface between the matching,
    the writing and the webendpoint.

    Therefor It provides methods that allow the webinterface to request
    parsed matches and return the selection made by the user.
    """

    coicop_levels = [5]
    matched_levels = {3: [1, 2], 4: [1, 2, 3], 5: [2, 3]}

    header = ["Name", "Code", "Level", "Matching step"]
    matches = None

    def __init__(self, matcher: matcher.Matcher,
                 schema: reader.waegungsschema.Waegungsschema,
                 writer: writer.Writer) -> None:

        self.matcher = matcher
        self.COICOP = schema
        self.writer = writer
        self.coicop = None

    def check_further(self) -> bool:
        """
        This method checks if there is a further COICOP number to match.

        To do so it requrests a new COICOP object and if one is returned this
        is stored in the COICOP attribute.
        :return: Is there a further coicop to match.
        """

        coicop = self.COICOP.get_coicop()
        while coicop is not None:
            if coicop.level in self.coicop_levels:
                if not self.writer.has_coicop(coicop.code):
                    self.coicop = coicop
                    return True
            coicop = self.COICOP.get_coicop()
        return False

    def get_selection(self) -> Dict:
        """
        This method gets a new selection

        if None is returned there is nothing more to do.
        :return:
        """

        # first match the coicop number.
        print(self.coicop.name)
        matches = self.matcher.match(self.coicop.name)

        # put the matches into one list.
        if matches is not None:
            matches = self.structure_matches(matches, self.coicop.level)
        else:
            matches = [[]]

        # next get the coicop data.
        tmp_coicop = {'name': self.coicop.name, 'level': str(self.coicop.level)}
        return {'Matches': matches, 'COICOP': tmp_coicop}

    def structure_matches(self, matches, coicop_level):
        """

        :param matches: the matches as returned from the matcher.
        :return:
        """

        tmp_matches = []
        for i in range(len(matches)):
            tmp = matches[i]
            if tmp[0].level in self.matched_levels[coicop_level]:
                tmp_matches.append([str(tmp[0].name), str(tmp[0].code), str(tmp[0].level), str(tmp[1])])
        self.matches = sorted(tmp_matches, key=lambda x: (x[3], x[2]))
        # add the id row.
        tmp_matches = []
        for i in range(len(self.matches)):
            tmp_matches.append([str(i)] + self.matches[i])
        result = [['id'] + self.header]
        result = result + tmp_matches


        return result

    def write_signal(self):
        """
        This method passes the write signal to the writer.
        """
        self.writer.write()

    def write_data(self, data: List[int]) -> None:
        print("write data called")
        naces = [self.matches[i] for i in data['table']]
        self.writer.set_coicop(self.coicop.code, self.coicop.name,
                               naces, data['manual'])


