Code Reference
==============


Matcher
"""""""

.. autoclass:: matcher.Matcher
    :members:


Reader
""""""

.. autoclass:: reader.abstractreader.AbstractReader
    :members:

.. autoclass:: reader.waegungsschema.Waegungsschema
    :members:

.. autoclass:: reader.nace.Nace
    :members:
