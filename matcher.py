from typing import List
from fuzzywuzzy import fuzz


from reader.nace import NaceEntry


class Matcher:
    """
    This class matches one string to the NACE codes.

    It exposes one method, :meth:`~matcher.Matcher.match`, that uses several different matching
    strategies, that allow for progressively more errors to find a list of possible
    NACE candidates.

    It does not list these classes, nor performs any sanity checks, this must be done
    in other classes.
    """

    def __init__(self, nace: List[NaceEntry], common: List[str],
                len_cutoff: int=5, fuzzy_cutoff: int=90,
                fuzzy_partial_cutoff: int=95, fuzzy_ratio_cutoff: int=95) -> None:
        """
        It requires both the NACE codes and the common list from the nace reader.

        All cutoff values are on a scale of 0 to 100, and the match is accepted
        when the match is greater or equal to the cutoff value.

        :param nace: The NACE list. Coming from the reader.
        :type nace: `List[NaceEntry]`
        :param common: The list of common tokens also returned from the reader.
        :type common: `List[str]`
        :param len_cutoff: The number of characters that a string requires to be matched.
        :type len_cutoff: int
        :param fuzzy_cutoff: The cutoff value for fuzzy matching
        :type fuzzy_cutoff: int
        :param fuzzy_partial_cutoff: The cutoff for partial fuzzy matching
        :type fuzzy_partial_cutoff: int
        :param fuzzy_ratio_cutoff: The cutoff for fuzzy set matching.
        :type fuzzy_ratio_cutoff: int
        """

        self.nace = nace
        self.common = common
        self.len_cutoff = len_cutoff
        self.fuzzy_cutoff = fuzzy_cutoff
        self.fuzzy_partial_cutoff = fuzzy_partial_cutoff
        self.fuzzy_ratio_cutoff = fuzzy_ratio_cutoff

    def match(self, good: str) -> List[NaceEntry]:
        """
        This method is called with the string and it returns a list of possible matches.

        I iterate through all NACE codes and applies an internal matching function on it.
        The matching is based on the tokens with the common words removed.

        It uses a following proceedures, that allow for increasingly more errors:
        1. exact matching.
        2. all tokens match exactly.
        3. fuzzy matching, using self.fuzzy_cutoff
        4. partial fuzzy matching using fuzzy_partial_cutoff
        5. matching using token set ratio

        If the matching was not successful the function returns zero and
        any goods with less than or equal to self.len_cutoff characters won't be matched.

        :param good: The good for which the nace code is searched.
        :return: A list of possible nace codes.
        """

        good_tokens = [word for word in good.lower().split(' ') if word not in self.common]
        good_tokens = ' '.join(good_tokens)
        print(good_tokens)

        results = []
        for nace in self.nace:
            res = self._match(nace.token, good_tokens)
            if res != 0:
                results.append([nace, res])

        return results

    def _match(self, nace: str, good: str) -> int:
        """
        This  function perforrms the internal matching.

        :param nace: The nace token string that is matched against
        :type nace: str
        :param good: The good token string that I want to match.
        :type good: str
        :return: The code of the matching result, lower is better, 0 means no matching.
        :rtype: int
        """

        if len(good) <= self.len_cutoff:
            return 0

        # first exact matching
        if nace == good:
            return 1

        # next compare all tokens.
        # for this I need to first extract all tokens and then sort them.
        token_nace = sorted(nace.split(' '))
        token_good = sorted(good.split(' '))
        if ' '.join(token_nace) == ' '.join(token_good):
            return 2

        # now a simple fuzzy match.
        if fuzz.ratio(good, nace) >= self.fuzzy_cutoff:
            return 3

        if fuzz.partial_ratio(good, nace) >= self.fuzzy_partial_cutoff:
            return 4

        if fuzz.token_set_ratio(good, nace) >= self.fuzzy_ratio_cutoff:
            return 5

        return 0


