library(jsonlite)

#####
##### Fixing the names in the nace
#####
#
# These two functions replace the messed up names.
# My first version wrote the name of the nace in the code.
match_nace <- function(tmp, naces_in, coicop) {
    if ("name_fixed" %in% names(tmp)) {
        return(tmp)
    }

    if (tmp$rejected) {
        return(tmp)
    }

    naces <- list()

    # first I need to extract the raw nace list and remove all names.
    naces_raw <- unlist(tmp$nace)
    naces <- naces_raw[names(naces_raw) == "code"]
    naces_out <- character()
    for (i in 1:length(naces)) {
        if (grepl("^[a-zA-Z, ]*$", naces[i])) {
            # This entry is a name of a class and I must match it to a code
            naces_out <- append(naces_out, naces_in$code[naces_in$name == naces[i]][1])
        }
        else {
            # This already is a code and I'm good.
            naces_out <- append(naces_out, naces[i])
        }
    }

    if (length(naces_out) == 0) {
        print(paste("Something failed for", coicop))
        tmp$rejected <- TRUE
    }

    tmp$nace <- naces_out
    return(tmp)
}

fix_names <- function(dat) {

    tmp_naces <- read.csv("nace.csv", header = F)
    names(tmp_naces) <- c("code", "name")
    names_dat <- names(dat)
    results <- list()
    for (i in 1:length(dat)){
        results[[i]] <- match_nace(dat[[i]], tmp_naces, names_dat[i])
        results[[i]]$coicop <- names_dat[i]
    }
    return(results)
}


#####
##### This function converts the json entry to a data frame
#####

entry_to_df <- function(coicop_dat) {
    res <- data.frame("coicop" = coicop_dat$coicop,
                      "nace" = unlist(coicop_dat$nace))
    return(res)
}

build_result <- function(dat) {

    results = data.frame("coicop" = character(),
                         "nace" = character())
    for (i in 1:length(dat)) {
        print(i)
        if (!dat[[i]]$rejected) {
            results <- rbind(results, entry_to_df(dat[[i]]))
        }
    }

    return(results)
}

#####
##### calling the code to fix the naces.
#####

raw_dat <- read_json("matches.json")
rd2 <- fix_names(raw_dat)
write.csv(res, file = "results.csv")
