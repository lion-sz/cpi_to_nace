import pandas
import camelot
import logging
import os.path


class AbstractReader:

    lazy_file = "hi.csv"

    def __init__(self, path: str, lazy: bool = False, page_start: int = 1, page_end: int = 0,
                 level_cutoff: int = 5) -> None:
        """
        This class provides methods to read in the tables.

        When the class is called in lazy mode it will try to read in a local copy of the already parsed data.
        If this copy does not exist it will parse the tables and create it.

        :param path: The filepath where the nace file can be found
        :type path: str
        :param page_start: The first page that contains the tables.
        :param page_start: int
        :param page_end: The last page with a table on it. If set to 0, end is used instead.
        :type page_end: int
        :param level_cutoff: The cutoff level. All ids, that are longer than this are removed
            from the dataframe. This is only used in the Waegungsschema class.
        :type level_cutoff: int
        :param lazy: If the class should try to read in an already processed version.
        :type lazy: bool
        """
        self.path = path
        self.lazy = lazy
        self.page_start = page_start
        self.page_end = page_end if page_end > page_start else 'end'
        self.level_cutoff = level_cutoff

        self.logger = logging.getLogger(self.logger_name)
        self.tables = []
        self.table = None
        self._i = 0

    def read(self) -> pandas.DataFrame:
        """
        This method reads in the file.
        It uses other methods internally, is however the only method exposed.

        :return: The read in and processed Tables, as a single DataFrame
        :rtype: `pandas.DataFrame`
        """

        # if lazyloading is requested, try it.
        if self.lazy:
            if self._lazy_load():
                return self.table

        # read in the tables.
        # I can skip the first page
        pages = '{}-{}'.format(self.page_start, self.page_end)
        self._tables_raw = camelot.read_pdf(self.path, flavor='stream', pages=pages)

        self._process()

        self.table = pandas.concat(self.tables)
        self.table.index = range(self.table.shape[0])

        # if lazy loading was enabled and I made it here the file did not exist.
        # Therefor I creat it.
        if self.lazy:
            self.table.to_csv(self.lazy_file)
        return self.table

    def _lazy_load(self) -> bool:
        """
        This method tries to load the already parsed file from disk.
        :return: Did loading work?
        :rtype: bool
        """

        if os.path.isfile(self.lazy_file):
            self.table = pandas.read_csv(self.lazy_file, dtype={'id': str})
            return True
        return False

    def _process(self) -> None:
        """
        This function populates the tables array.
        The actual processing is done by other methods, that are called here.
        The processing steps are:

        * Remove the unneeded header
        * Check if the automatic line split worked right.
        * Remove an rows that are to detailed.
        """

        for i in range(self._tables_raw.n):
            no_head = self._dehead(self._tables_raw[i].df,
                                   self._tables_raw[i].page)
            if no_head is None:
                continue
            self.tables.append(self._post_process_table(no_head, str(i + 2)))

    def _dehead(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        This method removes the header from the tables.

        :param table: The table returned from camelot
        :type table: 'pandas.DataFrame'
        :param page: The page number, passed to the error messages
        :type page: str
        """
        return table

    def _post_process_table(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        This method does some further processing.

        :param table: The deheaded table
        :type table: `pandas.DataFrame`
        :param page: The page number. This is written to the logger in the case of an error.
        :type page: str
        """
        return table
