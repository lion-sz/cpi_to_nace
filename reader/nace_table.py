import camelot
import pandas
import logging


from reader import abstractreader


class NACE(abstractreader.AbstractReader):

    table_names = ['division', 'group', 'class', 'desc', 'revision']

    lazy_file = "./nace_table.csv"

    logger_name = "NACE"

    def _dehead(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        The tables in the Schema contain a header, which I need to remove.

        The first entry after the header has always a Division set.
        Therefor I go through the division column and look for something that I can cast to int.
        If this is the case I found the first line.

        :param table: The table returned from camelot
        :type table: 'pandas.DataFrame'
        :param page: The page number, passed to the error messages
        :type page: str
        """

        nrow = table.shape[0]

        # go through the first 10 rows and look for the end of the header.
        header_end = None
        for i in range(0, min(10, nrow)):
            cell = table.at[i, 0].strip()
            if cell.isnumeric():
                header_end = i
                break
        if header_end is None:
            self.logger.error("Failed to parse page {}".format(page))
            return None
        else:
            self.logger.debug("Header on page {} extends to row {}".format(page, header_end - 1))

        # now remove those
        headless = table.drop(range(0, header_end))

        # and recreate the index
        nrow = headless.shape[0]
        headless.index = range(0, nrow)

        # set the column names
        headless.columns = self.table_names

        return headless

    def _post_process_table(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        Sometimes (right now only on the last page) camelot finds the footer to be a table.
        Therefor I need to remove this line.

        Since the footer contains the string 'NACE', I can simply remove all rows where this occurs.

        :param table: The table without its header.
        :type table: `pandas.DataFrame`
        :param page: The page number.
        :type page: str
        :return: The processed dataframe, ready to be joined.
        :rtype: `pandas.DataFrame`
        """

        to_drop = []
        for i in range(table.shape[0]):
            if 'NACE' in ''.join(table.values[i]):
                to_drop.append(i)

        if len(to_drop) > 0:
            result = table.drop(index=to_drop)
            result.index = range(result.shape[0])
        else:
            result = table

        return result
