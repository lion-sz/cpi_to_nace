import camelot
import pandas
import logging
from typing import NamedTuple


from reader import abstractreader


class COICOP(NamedTuple):
    name: str
    code: str
    level: int


class Waegungsschema(abstractreader.AbstractReader):

    table_header = ['', 'Bezeichnung / item', 'Verbraucherpreisindex insgesamt']
    """
    This list contains all strings that I associate with a header.
    """

    table_names = ['id', 'ger', 'en', 'weight']
    """
    These are the new names that I assign to the table.
    """

    logger_name = "Waegungsschema"

    lazy_file = "./schema.csv"

    def _dehead(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        The tables in the Schema contain a header, which I need to remove.
        The header is usually 3 rows long, with the second column having no content
        in this rows (or at most the string 'Bezeichnung / item').

        Therefor I need this column to check when the header ends.

        One small extra is that the first table contains the line
        'Verbraucherpreisindex insgesamt', which is also useless and removed.

        :param table: The table returned from camelot
        :type table: 'pandas.DataFrame'
        :param page: The page number, passed to the error messages
        :type page: str
        """

        # go through the first 10 rows and look for the end of the header.
        header_end = None
        for i in range(0, 10):
            cell = table.at[i, 1].strip()
            if cell not in self.table_header:
                header_end = i
                break
        if header_end is None:
            self.logger.error("Failed to parse page {}".format(page))
            return None
        else:
            self.logger.debug("Header on page {} extends to row {}".format(page, header_end - 1))

        # now remove those
        headless = table.drop(range(0, header_end))

        # and recreate the index
        nrow = headless.shape[0]
        headless.index = range(0, nrow)

        return headless

    def _post_process_table(self, table: pandas.DataFrame, page: str) -> pandas.DataFrame:
        """
        There are two major tasks to do on the dataframe.

        First for some rows the split between the first and the second column is not clear.
        Since the first column contains only numbers I can manually split the strings,
        when I find a space in the first column.

        Second I need to calculate a level column and remove all rows that are to detailed.
        This is controlled by the level_cutoff argument.

        :param table: The deheaded table
        :type table: `pandas.DataFrame`
        :param page: The page number. This is written to the logger in the case of an error.
        :type page: str
        """

        # get table dimensions
        nrow = table.shape[0]

        # since there are two conditions justifying a drop I declare the array here.
        ind_drop = []

        # first check the splits:
        #   Iterate through the rows and check if there is a space
        for i in range(0, nrow):
            if ' ' in table.at[i, 0]:
                self.logger.info("Failed automatic split detected. Correcting.")
                (left, right) = table.at[i, 0].strip().split(' ', maxsplit=1)
                if len(left) <= 5:
                    self.logger.warning("Automatic split failed for level below 5: {}. Dropping row."
                                        .format(table.at[i, 0]))
                ind_drop.append(i)
                table.at[i, 0] = left
                table.at[i, 1] = right

        # and now calculate the levels
        levels = []
        for i in range(0, nrow):
            levels.append(len(table.at[i, 0]))
        table['level'] = levels

        # and now remove all elements that are above the cutoff level.
        for i in range(0, nrow):
            if table['level'][i] > 5:
                ind_drop.append(i)
        # drop and recalculate the index
        processed = table.drop(index=ind_drop)
        nrow = processed.shape[0]
        processed.index = range(0, nrow)

        return processed

    def get_coicop(self):
        """
        :return: This method returns a CCOICOP number
        """
        if self._i < self.table.shape[0]:
            coicop = COICOP(self.table['en'][self._i],
                            self.table['id'][self._i],
                            self.table['level'][self._i])
            self._i += 1
            return coicop
        else:
            return None
















