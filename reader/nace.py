import csv
from typing import NamedTuple, List


class NaceEntry(NamedTuple):
    name: str
    code: str
    token: str
    level: int


class Nace:

    def __init__(self, path="./nace.csv", n:int = 50) -> None:
        """
        This class reads in the file nace.csv and parses the entries.

        It removes the n most common tokens from the descriptions to make
        matching easier.

        :param path: The filepath
        :type path: str
        :param n: The number of the most common tokens to be removed.
        :type n: int
        """

        self.path = path
        self.n = n
        self.print_all_tokens = "hi"

    def read(self) -> None:
        """
        This method reads in the nace files and prepares the tokens.

        Internally  the work is done by other functions.

        :return:
        """

        with open(self.path, 'r') as csvfile:
            reader = csv.reader(csvfile)
            content = [row for row in reader]

        self.common = self._extract_common_tokens(content)

        self.entries = self._parse(content, self.common)

        return self.entries

    def _extract_common_tokens(self, file_content: List[List[str]]) -> List[str]:
        """
        This method extracts the n most common tokens from the file content.
        These can be removed later on.

        If the attribute print_all_tokens is set to a string the method
        will print all tokens and their count to this file.

        :param file_content: The content of the file, returned from the csv reader.
        :type file_content: str[][]
        :return: A list of n common strings that can be removed.
        """

        one_string_content = ' '.join([line[1] for line in file_content])
        tokens = one_string_content.lower().split(' ')

        token_dict = {}
        for token in tokens:
            token_dict[token] = token_dict.get(token, 0) + 1

        # sort the dict by the value into a list.
        sorted_tokens = sorted(token_dict.items(), reverse=True, key=lambda x: x[1])

        if self.print_all_tokens is not None:
            with open(self.print_all_tokens, 'w') as ofile:
                writer = csv.writer(ofile)
                writer.writerows(sorted_tokens)

        return [token[0] for token in sorted_tokens[0:self.n]]

    def _parse(self, file_content: List[List[str]], common: List[str]) -> List[NaceEntry]:
        """
        This method parses the file content in to an array of NaceEtnries.

        :param file_content: The content of the nace file, from the reader.
        :param common: The list of tokens that should be removed.
        :return: A list of NaceEntreis.
        :rtype: List[NaceEntry]
        """

        entries = []
        for row in  file_content:
            # first I need to find all tokens that remain
            tokens = [word for word in row[1].lower().split(' ') if word not in common]
            entries.append(NaceEntry(row[1], row[0], ' '.join(tokens), self._calc_level(row[0])))

        return entries

    def _calc_level(self, code: str) -> int:
        """
        This methods calculates the level given one code string.

        The levels that I use are:
        * 0 - Section: Only one Letter.
        * 1 - Division: Longer than one Letter, but no dot inside.
        * 2 - Group: exactly one dot inside the string.
        * 3 - Class: two dots in the string.

        :param code:  The code for which the level should be calculated.
        :type code: str
        :return: The level of the code. See the definition above.
        :rtype: int
        """

        # first the section; these contain only one letter.
        if len(code) == 1:
            return 0
        # If there is no dot in the code this is a division.
        if '.' not in code:
            return 1
        # if there is only one dot this is a group, otherwise a class.
        if len(code.split('.')) == 2:
            return 2
        else:
            return 3