async function further_data() {
    let response = await fetch('/next');
    let data = await response.json();
    return data;
}

async function get_data() {
    let response = await fetch('/data');
    let data = await response.json();
    return data;
}

function add_coicop(data) {
    document.getElementById('coicop_name').innerHTML = "name: " + data.name
    document.getElementById('coicop_level').innerHTML = "level: " + data.level
}

function request_write() {
    // This function calls the endpoint that sends a write signal.
    fetch("/write_signal")
}

function table_create(data){
    var body = document.body,
        tbl  = document.getElementById('matching_table');
//    tbl.style.width  = '100px';
    tbl.style.border = '1px solid grey';
    nrows = data.length
    ncols = data[0].length

    for(var i = 0; i < nrows; i++){
        var tr = tbl.insertRow();
        for(var j = 0; j < ncols; j++){
            var td = tr.insertCell();
            td.appendChild(document.createTextNode(data[i][j]));
            td.style.border = '1px solid grey';
        }
    }
    body.appendChild(tbl);
}

async function send_selection() {

    input_field = document.getElementById('selection')
    vals = input_field.value.split(',')

    var selection = []
    for (i = 0; i < vals.length; i++) {
        if (isNaN(vals[i])) {
            continue
        }
        if (vals[i] == '') {
            continue
        }
        selection.push(Number(vals[i]))
    }

    manual_input_field = document.getElementById('manual_codes')
    manual_input = manual_input_field.value.split(',')

    res = await fetch("/selection",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify({'table': selection, 'manual': manual_input})
    })
    console.log(await res.text())

    // Now I want to reset the input field
    input_field.value = ''
    manual_input_field.value = ''

    window.location.reload()
}

function reset_color(tab, nrows) {
    for (i = 0; i < nrows; i++) {
        tab.rows[i].bgColor = 'white'
    }
}

function check_send(event) {
    // This function simply checks for the enter key
    // and if found calls the send selection function.
    if (event.keyCode == 13) {
        send_selection()
        return
    }
}
function color_table(event) {
    // On enter send the selection.
    if (event.keyCode == 13) {
        send_selection()
        return
    }

    // otherwise color the table
    input_field = document.getElementById('selection')
    tab = document.getElementById('matching_table')
    nrows = tab.rows.length - 1
    reset_color(tab, nrows)
    val = input_field.value
    vals = val.split(',')

    for (i = 0; i < vals.length; i++) {
        // first I need to check if this is a number, or an empty string.
        if (vals[i] == "") {
            continue
        }
        tmp = Number(vals[i])
        if (tmp > nrows) {
            alert('exceeding table row limit!')
        }
        else {
            tab.rows[tmp + 1].bgColor = 'lightblue'
        }
    }
}

further_data().then(further => {
    if (further.response) {
        get_data().then(data => {
            add_coicop(data.COICOP);
            table_create(data.Matches);
        });
    }
});