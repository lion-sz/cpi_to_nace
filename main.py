import logging

import reader.waegungsschema
import reader.nace
import matcher
import writer
import interface.interface
import interface.webserver
#import reader.nace_table


SCHEMA_PATH = "waegungsschema-2015.pdf"
NACE_PATH = "nace_rev_2.pdf"


logger = logging.getLogger()
lazy = True


# read in the Schema
schema = reader.waegungsschema.Waegungsschema(SCHEMA_PATH, lazy=lazy, page_start=2)
schema.read()

nace = reader.nace.Nace(n=20)
nace.read()
# read in the nace Table
#nace = reader.nace_table.NACE(NACE_PATH, lazy=lazy, page_start=63, page_end=92)
#nace.read()

match = matcher.Matcher(nace.entries, nace.common, fuzzy_cutoff=60,
                        fuzzy_partial_cutoff=70, fuzzy_ratio_cutoff=80,
                        len_cutoff=3)
writ = writer.Writer()
writ.read()

inter = interface.interface.Interface(match, schema, writ)
server = interface.webserver.WebInterface(__name__, inter)

server.run()

